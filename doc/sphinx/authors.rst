.. _authors:

Authors
=======

Current maintainers
-------------------

- Zamaan Raza
- Christian Trageser
- Ammar Nejati
- Joachim Wuttke

Scientific contributors
-----------------------

- Andreas Ostermann
- Tobias Schrader

Previous contributors
---------------------

- Laurent Chapon
- Prachi Chavda
- Jonathan M. Fisher
- Jamie Hall
- Janike Katter
- Eric Pellegrini
- Matthias Puchner
- Navid Qureshi
- Alexander Schober
- Tobias Weber
