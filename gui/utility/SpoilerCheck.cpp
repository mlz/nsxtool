#include <QCheckBox>
#include <QPropertyAnimation>
#include <iostream>

#include "SpoilerCheck.h"

SpoilerCheck::SpoilerCheck(const QString& title) : Spoiler(title, true) { }
