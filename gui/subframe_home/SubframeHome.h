//  ***********************************************************************************************
//
//  OpenHKL: data reduction for single crystal diffraction
//
//! @file      gui/subframe_home/SubframeHome.h
//! @brief     Defines class SubframeHome
//!
//! @homepage  https://openhkl.org
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Institut Laue-Langevin and Forschungszentrum Jülich GmbH 2016-
//! @authors   see CITATION, MAINTAINER
//
//  ***********************************************************************************************

#ifndef OHKL_GUI_SUBFRAME_HOME_SUBFRAMEHOME_H
#define OHKL_GUI_SUBFRAME_HOME_SUBFRAMEHOME_H

#include "gui/models/ExperimentModel.h"
#include "gui/subwindows/InputFilesWindow.h"
#include "gui/subwindows/PeaklistWindow.h"
#include "gui/views/ExperimentTableView.h"

#include <memory>

#include <QHBoxLayout>
#include <QListWidget>
#include <QPair>
#include <QPushButton>
#include <QSplitter>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QWidget>

class InputFilesWindow;
class InstrumentDataWindow;

//! Frame containing interface to create, save and load experiments
class SubframeHome : public QWidget {
 public:
    //! Default constructor
    SubframeHome();

 private:
    //! Set the left layout
    void _setLeftLayout(QHBoxLayout* main_layout);
    //! Set the right layout
    void _setRightLayout(QHBoxLayout* main_layout);

 private:
    //! Switch the current experiment through the model index
    void _switchCurrentExperiment(const QModelIndex& index);
    //! Update the current list
    void _updateLastLoadedList(QString name, QString file_path);
    //! Update the current list widget
    void _updateLastLoadedWidget();
    //! Load the clicked item
    void _loadSelectedItem(QListWidgetItem* item);

 public:
    //! Create a new experiment
    void createNew();
    //! Load an experiment from file
    void loadFromFile();

    //! Save the current experiment
    void saveCurrent(bool dialogue = false);
    //! Save all experiments provided they have save paths (not implemented)
    void saveAll();
    //! Remove the current experiment
    void removeCurrent();

    //! Read the settings
    void readSettings();
    // ! Save the settings
    void saveSettings() const;

    //! Disable unsafe widgets if no data loaded
    void toggleUnsafeWidgets();
    //! Refreshing tables on SubframeHome
    void refreshTables() const;
    //! Clearing tables on SubframeHome
    void clearTables();
    //! Setup ContextMenu on Dataset table
    void setContextMenuDatasetTable(QPoint pos);
    //! Setup ContextMenu on PeakCollection table
    void setContextMenuPeakTable(QPoint pos);
    //! Setup ContextMenu on UnitCell table
    void setContextMenuUnitCellTable(QPoint pos);

 private:
    QPushButton* _new_exp;
    QPushButton* _old_exp;
    QPushButton* _save_current;
    QPushButton* _save_all;
    QPushButton* _remove_current;
    QPushButton* _show_input_files;
    QPushButton* _show_found_peaks;

    ExperimentTableView* _open_experiments_view;
    std::unique_ptr<ExperimentModel> _open_experiments_model;

    QList<QPair<QString, QString>> _last_experiments;
    QListWidget* _last_import_widget;

    QTableWidget* _dataset_table;
    QTableWidget* _peak_collections_table;
    QTableWidget* _unitcell_table;
};

#endif // OHKL_GUI_SUBFRAME_HOME_SUBFRAMEHOME_H
